function Reincarnation(keys)
	local caster = keys.caster
	local casterMana = caster:GetMana()
	local health = caster:GetHealth()

	local ability = keys.ability
	local cooldown = ability:GetCooldown(ability:GetLevel() - 1)
	local abilityManaCost = ability:GetManaCost( ability:GetLevel() - 1 )

	local reincarnate_time = ability:GetLevelSpecialValueFor( "reincarnate_time", ability:GetLevel() - 1 )
	local slow_radius = ability:GetLevelSpecialValueFor( "slow_radius", ability:GetLevel() - 1 )

	if ability:IsCooldownReady() and casterMana >= abilityManaCost then
		if not caster:HasModifier("modifier_reincarnation_min_health") then
			ability:ApplyDataDrivenModifier(caster, caster, "modifier_reincarnation_min_health", nil)
		end
	else
		if caster:HasModifier("modifier_reincarnation_min_health") then
			caster:RemoveModifierByName("modifier_reincarnation_min_health")
		end
	end

	if caster:HasModifier("modifier_reincarnation_min_health") then
		--ability:ApplyDataDrivenModifier(caster, caster, "modifier_reincarnation_min_health")
		if health == 1 then
			ability:ApplyDataDrivenModifier(caster, caster, "modifier_reincarnation_reincarnating", {duration = 3.0})
			caster:RemoveModifierByName("modifier_reincarnation_min_health")
			ability:StartCooldown(cooldown)

			-- Sounds
			caster:EmitSound("Hero_SkeletonKing.Reincarnate")
			caster:EmitSound("Hero_SkeletonKing.Death")
			Timers:CreateTimer(reincarnate_time, function()
				caster:EmitSound("Hero_SkeletonKing.Reincarnate.Stinger")
			end)

			-- Slow
			local enemies = FindUnitsInRadius(caster:GetTeamNumber(), caster:GetAbsOrigin(), nil, slow_radius,
										DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
										DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

			for _,unit in pairs(enemies) do
				ability:ApplyDataDrivenModifier(caster, unit, "modifier_reincarnation_slow", nil)
			end
		end
	end
end
